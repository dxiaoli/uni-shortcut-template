import App from './App'
import uviewPlus from '@/uni_modules/uview-plus'
// 全局mixin
import mixin from "@/utils/mixin.js";
// http
import http from '@/utils/request.js'
// pinia
import store from './store'
import {
	createSSRApp
} from 'vue'
export function createApp() {
	const app = createSSRApp(App)
	app.use(uviewPlus)
	app.use(store)
	app.mixin(mixin)
	app.config.globalProperties.$http = http
	return {
		app
	}
}
