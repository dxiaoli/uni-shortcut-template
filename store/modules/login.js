import {
	defineStore
} from 'pinia'
import {
	ref
} from 'vue'

const loginStore = defineStore('book', {
	state: () => {
		return {
			uinfo: '',
			bookList: []
		}
	},
	actions: {
		login(e) {
			this.uinfo = e
		},
		setBookList(e) {
			this.bookList = e
		}
	},
	persist: {
		enabled: true,
		H5Storage: window?.localStorage,
		strategies: [{
			storage: window?.localStorage
		}],
		storage: [{
			storage: window?.localStorage
		}]
	}
})

export default loginStore