let timeout = null;
const baseMixin = {
	data() {},
	methods: {
		// 提示公共类 title：提示内容 duration：显示时长
		sToast(title, duration = 2000) {
			uni.showToast({
				title,
				icon: 'none',
				duration
			})
		},
		// 返回上一页公共类
		back(delta = 1, time = 0) {
			setTimeout(function() {
				let pages = getCurrentPages();
				if (pages[pages.length - 2]) {
					uni.navigateBack({
						delta
					})
				} else {
					uni.switchTab({
						url: '/pages/index/index'
					})
				}

			}, time)
		},
		// 打开新页面公共类 0普通跳转  1底部跳转 2关闭当前页面跳转
		open(url, type = 0, time = 0) {
			setTimeout(function() {
				if (type == 0) {
					uni.navigateTo({
						url: url
					})
				} else if (type == 1) {
					uni.switchTab({
						url: url
					})
				} else if (type == 2) {
					uni.reLaunch({
						url: url
					})
				} else {
					uni.redirectTo({
						url: url
					})
				}
			}, time)
		},
		// 打开新页面公共类 0普通跳转  1底部跳转 2关闭当前页面跳转
		showModal(content, title = '温馨提示', showCancel = true, confirmText = '确定',
			confirmColor = "#0378c7") {
			return new Promise((resolve, reject) => {
				uni.showModal({
					title,
					content,
					showCancel,
					confirmText,
					confirmColor,
					success(e) {
						if (e.confirm) {
							resolve(true)
						} else {
							reject(false)
						}
					}

				})
			})
		},
		/**
		 * 防抖原理：一定时间内，只有第一次操作，再过wait毫秒后才执行函数
		 * 
		 * @param {Function} func 要执行的回调函数 
		 * @param {Number} wait 延时的时间
		 * @return null
		 */
		debounce(func, wait = 500) {
			// 清除定时器
			if (timeout !== null) clearTimeout(timeout);
			var callNow = !timeout;
			timeout = setTimeout(function() {
				timeout = null;
			}, wait);
			if (callNow) typeof func === 'function' && func();
		},

		// 生成n位随机数
		randomn(n) {
			if (n > 21) return null
			return parseInt((Math.random() + 1) * Math.pow(10, n - 1))
		},

		// 获取当前时间戳
		getTime(type = 1) {
			return parseInt(new Date().getTime() / type)
		},

		// 深拷贝
		copy(obj) {
			let newObj = null
			if (typeof obj === 'object' && obj !== null) {
				newObj = obj instanceof Array ? [] : {}
				for (let i in obj) {
					newObj[i] = typeof obj[i] === 'object' ? this.copy(obj[i]) : obj[i]
				}
			} else {
				newObj = obj
			}
			return newObj
		},
		// 时间戳：1637244864707
		/* 时间戳转换为时间 */
		timestampToTime(timestamp) {
			timestamp = timestamp ? timestamp : null;
			let date = new Date(timestamp); //时间戳为10位需*1000，时间戳为13位的话不需乘1000
			let Y = date.getFullYear() + '-';
			let M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
			let D = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()) + ' ';
			let h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
			let m = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
			let s = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
			return Y + M + D + h + m + s;
		}
	}
}

export default baseMixin;